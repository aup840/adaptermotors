package app;
import motorNormal.Motor;
import adapters.MotorModernoAdapter;
import motorNormal.MotorComun;
import motorNormal.MotorEconomico;
import adapters.MotorNormalAdapter;
import motorModerno.MotorElectrico;
import motorModerno.MotorModerno;

public class Automovil {
	public void run(){
		Motor motor1 = new MotorComun();
		MotorModerno motorAdaptadoComun = new MotorNormalAdapter(motor1);	
		System.out.println("Operaciones con un motor moderno adaptado a un motor tradicional Com�n");		 
		motorAdaptadoComun.conectar();
		motorAdaptadoComun.moverMasRapido();
		motorAdaptadoComun.detener();
		System.out.println();
		
		MotorEconomico motor1a = new MotorEconomico();
		MotorModerno motorAdaptadoEcon = new MotorNormalAdapter(motor1a);	
		System.out.println("Operaciones con un motor moderno adaptado a un motor tradicional Econ�mico");		 
		motorAdaptadoEcon.conectar();
		motorAdaptadoEcon.moverMasRapido();
		motorAdaptadoEcon.detener();
		
		System.out.println();
		System.out.println("Operaciones con un motor com�n adaptado a un motor moderno");
		MotorModerno motor2 = new MotorElectrico ();
		Motor motorNvoViejo = new MotorModernoAdapter(motor2);
		motorNvoViejo.encender();
		motorNvoViejo.acelerar();
		motorNvoViejo.apagar();
	}
}
