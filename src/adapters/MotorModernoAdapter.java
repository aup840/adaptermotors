package adapters;
import motorNormal.Motor;
import motorModerno.MotorModerno;
	//	Motor Normal a Moderno
public class MotorModernoAdapter implements Motor {
	MotorModerno motor;
	
	public MotorModernoAdapter(MotorModerno  motor){
		this.motor= motor;
	}

	@Override
	public void encender() {
		motor.conectar();
		motor.activar();		
	}

	@Override
	public void acelerar() {
		motor.moverMasRapido();		
	}

	@Override
	public void apagar() {
		motor.detener();
		motor.desconectar();	
	}
}