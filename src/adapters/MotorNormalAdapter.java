package adapters;
import motorNormal.Motor;
import motorModerno.MotorModerno;;

//Motor electrico a normal
public class MotorNormalAdapter implements MotorModerno {
	Motor motor;

	public MotorNormalAdapter(Motor motor){
		this.motor = motor;
	}
	
	@Override
	public void conectar() {
		activar();	
	}

	@Override
	public void activar() {		
		motor.encender();
	}

	@Override
	public void moverMasRapido() {
		motor.acelerar();		
	}

	@Override
	public void detener() {
		desconectar();
	}

	@Override
	public void desconectar() {
		motor.apagar();		
	}
}