package motorNormal;

public class MotorComun implements Motor{
	@Override
	public void encender() {
		System.out.println("Se ha puesto en marcha el motor");		
	}

	@Override
	public void acelerar() {
		System.out.println("Se va adquiriendo velocidad");
	}

	@Override
	public void apagar() {
		System.out.println("Se ha detenido la marcha del motor");
	}
}
