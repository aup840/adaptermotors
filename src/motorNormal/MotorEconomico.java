package motorNormal;

public class MotorEconomico implements Motor{
	@Override
	public void encender() {
		System.out.println("Se ha puesto en marcha el motor");		
	}

	@Override
	public void acelerar() {
		System.out.println("Se va adquiriendo velocidad");
		System.out.println("Este motor ahorra el 20% de combustible");		
	}

	@Override
	public void apagar() {
		System.out.println("Se ha detenido la marcha del motor");
	}
}