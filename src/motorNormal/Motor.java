package motorNormal;

public interface Motor {
	public void encender();
	public void acelerar();
	public void apagar();
}
