package motorModerno;

public class MotorElectrico implements MotorModerno {
	public void conectar() {
		System.out.println("El motor se ha encendido");
	}

	@Override
	public void activar() {
		System.out.println("El motor se ha puesto en marcha");
	}

	@Override
	public void moverMasRapido() {
		System.out.println("El motor ha acelerado e incrementado su velocidad");	
	}

	@Override
	public void detener() {
		System.out.println("el motor se ha puesto en Stop y ha detenido la marcha");
	}

	@Override
	public void desconectar() {
		System.out.println("Se ha apagado el motor");		
	}
}